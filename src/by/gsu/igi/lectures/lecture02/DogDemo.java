package by.gsu.igi.lectures.lecture02;

/**
 * Created by Evgeniy Myslovets.
 */
public class DogDemo {

    public static void main(String[] args) {
        Dog sharpey = new Dog("Tuzik");

        sharpey.setAge(2);

        sharpey.barking();
        sharpey.sleeping();

        System.out.println("My name is " + sharpey.getName());

        sharpey.setAge(3);

        sharpey.sleeping();
    }
}
